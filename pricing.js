//mongo db 
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// Connection URL
const url = 'mongodb://localhost:27017';
// Database Name
const dbName = 'pricing';
var db;
// Create a new MongoClient
const client = new MongoClient(url,{ useUnifiedTopology: true });
// Use connect method to connect to the Server
client.connect(function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to the DB server.");
    db = client.db(dbName);
  });

// declaring the express parameters
var express = require("express");
var bodyParser = require('body-parser')
var path = require("path");


//express definition
var app = express();
var port = 8000;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false })) 
// parse application/json
app.use(bodyParser.json())
// path of the server directory
app.use(express.static("./"));
// form submission route
app.post("/submission",function(req,res) {
    //making the data object from the data recieved from the form in the req
    var today = new Date;
    var data={
        term : req.body.term,
        size : req.body.size,
        emailauth : req.body.emailauth,
        phonever : req.body.phonever,
        registereddevice : req.body.registereddevice,
        templatedevice : req.body.templatedevice,
        gbstored : req.body.gbstored,
        datastorevolumestored : req.body.datastorevolumestored,
        storagevolumestored : req.body.storagevolumestored,
        month : today.getMonth()
    }
    // this commented code makes new month document in the collection for
    // some reason it gives error but still creates a new document in the collection
    // maybe you guys can check what is wrong with this piece of code.
    
    // //find if a value exists
    // db.collection("consumption").find({ "_id": Number(data.month) }, { $exists: true }).toArray(function(err, doc){
    //     if (doc && doc.length){ 
    //         return res.json({message : `DB updated`});
    //     }
    //     else {
    // //inserting the data into the database
    //          db.collection("consumption").insertOne({"_id" : Number(data.month)}, function(err, resultObject) {
    //             if(!resultObject.result.ok || !resultObject.result.n || err) {
    //                 return res.json({message: "Error", response : false});
    //             }
    //     res.json({message :`DB updated`});
    // });
    // }})


// commit 1 comment 
    // i am updating a predefined document in the DB 
    //that i created with values initiated with 0
    // and then i update them depending upon 
    //the term and size given by the user ,
    // im taking month as a parameter and there 
    //will be 12 predefined documents with same 
    //fields for every month, these queries will take 
    //a month and add the updates in the respective months document   
    //tell me if theres anything to change or add.

    switch(data.term){
        // getting the parameters from the UI and getting them to DB in every term
        // each term has similar code 
        case "upload":
            //query
            db.collection("consumption").updateOne({_id : Number(data.month)},
            { $inc: {
                "volumeuploaded" : Number(data.size),
                "uploadoperations" : 1 ,
                "emailauthenications" : Number(data.emailauth),
                "phoneverifications" : Number(data.phonever),
                "registereddevices" : Number(data.registereddevice),
                "templatedevices" : Number(data.templatedevice),
                "gbstored" : Number(data.gbstored),
                "datastorevolumestored" : Number(data.datastorevolumestored),
                "storagevolumestored" : Number(data.storagevolumestored)
             }})
                return res.json({message : "database updated"});
            break;

        case "download": 
            //query
            db.collection("consumption").updateOne({_id : Number(data.month)},
                { $inc: {
                    "volumedownloaded" : Number(data.size),
                    "downloadoperations" : 1,
                    "emailauthenications" : Number(data.emailauth),
                    "phoneverifications" : Number(data.phonever),
                    "registereddevices" : Number(data.registereddevice),
                    "templatedevices" : Number(data.templatedevice),
                    "gbstored" : Number(data.gbstored),
                    "datastorevolumestored" : Number(data.datastorevolumestored),
                    "storagevolumestored" : Number(data.storagevolumestored)
                 }})
                 return res.json({message : "database updated"});
            break;

        case "write":
            //query
            db.collection("consumption").updateOne({_id : Number(data.month)},
                { $inc: {
                    "datastorewrites" : 1,
                    "emailauthenications" : Number(data.emailauth),
                    "phoneverifications" : Number(data.phonever),
                    "registereddevices" : Number(data.registereddevice),
                    "templatedevices" : Number(data.templatedevice),
                    "gbstored" : Number(data.gbstored),
                    "datastorevolumestored" : Number(data.datastorevolumestored),
                    "storagevolumestored" : Number(data.storagevolumestored)
                }})
                return res.json({message : "database updated"});
            break;

        case "read":
            //query
            db.collection("consumption").updateOne({_id : Number(data.month)},
                { $inc: {
                    "datastorereads" : 1,
                    "emailauthenications" : Number(data.emailauth),
                    "phoneverifications" : Number(data.phonever),
                    "registereddevices" : Number(data.registereddevice),
                    "templatedevices" : Number(data.templatedevice),
                    "gbstored" : Number(data.gbstored),
                    "datastorevolumestored" : Number(data.datastorevolumestored),
                    "storagevolumestored" : Number(data.storagevolumestored)
                 }})
                return res.json({message : "database updated"});
            break;

        case "ingress":
            //query
            db.collection("consumption").updateOne({_id : Number(data.month)},
                { $inc: {
                    "ingress" : 1, 
                    "ingressvolume" : Number(data.size),
                    "emailauthenications" : Number(data.emailauth),
                    "phoneverifications" : Number(data.phonever),
                    "registereddevices" : Number(data.registereddevice),
                    "templatedevices" : Number(data.templatedevice),
                    "gbstored" : Number(data.gbstored),
                    "datastorevolumestored" : Number(data.datastorevolumestored),
                    "storagevolumestored" : Number(data.storagevolumestored)
                }})
                return res.json({message : "database updated"});
            break;

        case "egress":
            //query
            db.collection("consumption").updateOne({_id : Number(data.month)},
                { $inc: {
                    "egress" : 1, 
                    "egressvolume" : Number(data.size),
                    "emailauthenications" : Number(data.emailauth),
                    "phoneverifications" : Number(data.phonever),
                    "registereddevices" : Number(data.registereddevice),
                    "templatedevices" : Number(data.templatedevice),
                    "gbstored" : Number(data.gbstored),
                    "datastorevolumestored" : Number(data.datastorevolumestored),
                    "storagevolumestored" : Number(data.storagevolumestored)
                }})
                return res.json({message : "database updated"});
            break;
    }
})

    // billing route
app.post("/bill",function(req,res) {
    // query to find out the month doc in the DB
    db.collection('consumption').find({"_id" : Number(req.body.month)} ).toArray((err, results) => {
       //throwing error if theres one
        if(err) throw err;
        // else calling a function that calculates bill
        // here results is an array of objects from the DB 
        // we only need the first element in the array 
        // as you see in the consoling of results array
        // console.log(results);
        billingfunction(results[0])
        });
});
    //billing function 
    // here ro is just an object which is obviously results[0] when its used in the bill route
function billingfunction(ro){
    // implementation of the whole pricing chart here.
    // bill variable that is rendered 
    var bill;
    //this statement checks the first part of the chart the free version 
    if( ro.emailauthenications < 100 &&
        ro.phoneverifications < 100 &&
        ro.gbstored < 200 && 
        ro.registereddevices < 100 && 
        ro.templatedevices < 10 &&
        ro.volumedownloaded < 1024 &&
        ro.downloadoperations < 1000 &&
        ro.uploadoperations < 500 && 
        ro.datastorewrites < 1000 &&
        ro.datastorereads < 2000 &&
        ro.ingressvolume < 1024 &&
        ro.egressvolume < 1024 && 
        ro.datastorevolumestored < 100 && 
        ro.storagevolumestored < 500){
        bill = 0;
    }
    // 2nd portion 25$ one.
    else {
        bill = 25;
        // all the checks below are for the unlimited part as mentioned in the chart
        if( ro.phoneverifications > 150){ // if phoneverifications exceeds 150
           var AdditionalPVs = ro.phoneverifications - 150; // calculating additional PVs
           console.log("Addtional PVs :" + AdditionalPVs)
           var PVbill = AdditionalPVs * 0.08; // multiplaying with its price per phone verification
           bill = bill + PVbill;// adding to the original bill
        } 
        if( ro.gbstored > 1024){
            var AdditionalMBS = ro.gbstored - 1024; //getting addtionals MBs
            var AdditionalGB = AdditionalMBS / 1024;//conversion into GB 
            console.log("Additional Gbs :" + AdditionalGB);
            var GBSbill = AdditionalGB * 0.026;
            bill = bill + GBSbill;
         }
         // same as above with different parameters
         if( ro.storagevolumestored > 5120){
            var newSVS = ro.storagevolumestored - 5120;
            var AdditionalSVS = newSVS / 1024;
            console.log("Additional SVs :" + AdditionalSVS);
            var SVSbill = AdditionalSVS * 0.026;
            bill = bill + SVSbill;
         }
         // same as above with different parameters
         if( ro.volumedownloaded > 25600){
            var newVD = ro.volumedownloaded - 25600;
            var AdditionalVD = newVD / 1024;
            console.log("Additional VD :" + AdditionalVD);
            var VDbill = AdditionalVD * 0.12;
            bill = bill + VDbill;
         }
         // same as above with different parameters
         if( ro.datastorevolumestored > 1024){
            var newDSVS = ro.datastorevolumestored - 1024;
            var AdditionalDSVS = newDSVS / 1024;
            console.log("Additional DSVS :" + AdditionalDSVS);
            var DSVSbill = AdditionalDSVS * 0.18;
            bill = bill + DSVSbill;
         }
         // same as above with different parameters
        if( ro.ingressvolume > 5120 ){
            var newIV = ro.ingressvolume - 5120;
            var AdditionalIV = newIV / 1024;
            console.log("Additional IV :" + AdditionalIV);
            var IVbill = AdditionalIV * 0.05;
            bill = bill + IVbill;
         } 
         // same as above with different parameters
        if( ro.egressvolume > 10240 ){
            var newEV = ro.egressvolume - 10240;
            var AdditionalEV = newEV / 1024;
            console.log("Additional EV :" + AdditionalEV);
            var EVbill = AdditionalEV * 0.08;
            bill = bill + EVbill;
        }
    
         //operations / writes limitations are a bit different
          if ( ro.downloadoperations > 50000 ){ //if exceeds the given limit
            var AdditionalDO = ro.downloadoperations - 50000; // how much it exceeds the limit
            console.log("Additional DOs: " + AdditionalDO);
            var ADO1 = AdditionalDO / 10000; //now calculating how much to charge as it was 0.004/10000 operations 
            var ADO2 = Math.round(ADO1); // this rounds off and only charges in case of 10000 operations only.
            var DObill = ADO2 * 0.004; // bill calculation
            bill = bill + DObill;// addition to original bill
          }
          // same as above with different limits and cost caps
          if ( ro.uploadoperations > 20000 ){ 
            var AdditionalUO = ro.uploadoperations - 20000;
            console.log("Additional UOs: " + AdditionalUO);
            var AUO1 = AdditionalUO / 10000;
            var AUO2 = Math.round(AUO1);
            var UObill = AUO2 * 0.05;
            bill = bill + UObill;
          }
          
          // same as above with different limits and cost caps
          if ( ro.datastorewrites > 20000){ 
            var AdditionalDW = ro.datastorewrites - 20000;
            console.log("Additional DWs: " + AdditionalDW);
            var DW1 = AdditionalDW / 100000;
            var DW2 = Math.round(DW1);
            var DWbill = DW2 * 0.18;
            bill = bill + DWbill;
          }
          
          // same as above with different limits and cost caps
          if ( ro.datastorereads > 50000 ){ 
            var AdditionalDR = ro.datastorereads - 50000;
            console.log("Additional DRs: " + AdditionalDR);
            var DR1 = AdditionalDR / 100000;
            var DR2 = Math.round(DR1);
            var DRbill = DR2 * 0.06;
            bill = bill + DRbill;
          }
}
console.log(`The bill is : ${bill}`);


}
// Error Handling for Express server
app.use(function(req, res) {
    console.log(`Error ${req.url}`);
    res.status(404).json({code:"Error", message: `Not found Link ${req.url}`})
});

// Start the Server
app.listen(port, function() {
    console.log("Server started.");
    console.log(`Listening on the port number: ${port}`)
});